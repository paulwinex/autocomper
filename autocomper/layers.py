import re
import nuke
from collections import defaultdict


class Layer:
    """
    Базовый класс слоя
    """
    backdrop_name = ''
    include_patterns = ()
    exclude_patterns = ()
    merge = True
    merge_method = 'plus'
    merge_to_main = True
    backdrop_bg_color = None
    local_post_nodes = False

    position_x_step = 150
    position_y_step = 150
    after_shuffle_nodes = ()
    after_merge_nodes = ()

    def __init__(self):
        self.layers = []
        self.nodes = []
        self.root = None
        self.lines = 0
        self.output_node = None

    def __str__(self):
        return self.backdrop_name

    def __repr__(self):
        return f'<{self.backdrop_name} ({len(self.layers)})>'

    @property
    def geo(self):
        return dict(
            root_x=self.root.xpos() + (self.root.screenWidth() // 2),
            root_y=self.root.ypos() + (self.root.screenHeight() // 2),
            right=max([node.xpos() + (node.screenWidth() // 2) for node in self.nodes]),
            bottom=max([node.ypos() + (node.screenHeight() // 2) for node in self.nodes])
        )

    def filter_layers(self, layers):
        filtered = []
        for lyr in layers:
            if any([pat.match(lyr) for pat in self.include_patterns]):
                if self.exclude_patterns:
                    if any([pat.match(lyr) for pat in self.exclude_patterns]):
                        continue
                filtered.append(lyr)
        return filtered

    def collect_layers(self, layers):
        self.layers.clear()
        self.layers.extend(list(self.filter_layers(layers)))
        return len(self.layers)

    def _ordered_layers(self):
        order = ['diffuse',
                 'specular',
                 'coat',
                 'sheen',
                 'sss',
                 'transmission',
                 'volume',
                 ]
        return sorted(self.layers, key=lambda x: order.index(x.split('_')[0]) if x.split('_')[0] in order else 100)

    def create_precomp_nodes(self, input_node, offset=None):
        # input dot
        if not offset:
            self.root = self.create_dot(input_node)
        else:
            self.root = self.create_dot(input_node, direction=self.RIGHT, distance=offset)
        # shuffle dots
        prev_dot = self.root
        channels_offset = 0
        for i, layer in enumerate(self._ordered_layers()):
            self.lines = i + 1
            if i == 0:
                chan_dot = self.create_dot(prev_dot, direction=self.DOWN)
            else:
                chan_dot = self.create_dot(prev_dot, direction=self.RIGHT)
                channels_offset += 1
            self.nodes.append(chan_dot)
            prev_dot = chan_dot
            shuffle = self.create_node_with_offset(nuke.nodes.Shuffle2, chan_dot, name=layer, postage_stamp=True,
                                                   in1=layer)
            self.nodes.append(shuffle)
            if not self.merge:
                continue
            if i == 0:
                mrg = shuffle
            else:
                angle_dot = self.create_dot(shuffle, distance=i)
                self.nodes.append(angle_dot)
                mrg = self.create_node_with_offset(nuke.nodes.Merge, [mrg, angle_dot], operation='plus')
                mrg.setXpos(self.root.xpos() + (self.root.screenWidth() // 2) - (mrg.screenWidth() // 2))
                mrg.setYpos(angle_dot.ypos() + (angle_dot.screenHeight() // 2) - (mrg.screenHeight() // 2))
                self.nodes.append(mrg)
            self.output_node = mrg
        self.create_backdrop(self.nodes, self.backdrop_name)

    # create backdrop
    def create_backdrop(self, nodes, label):
        if not nodes:
            raise NotGeneratedError
        selected = nuke.selectedNodes()
        if selected:
            for node in selected:
                node['selected'].setValue(False)
        from nukescripts import autoBackdrop
        for node in nodes:
            node['selected'].setValue(True)

        bd = autoBackdrop()
        bd['label'].setValue(label)
        if self.backdrop_bg_color:
            bd['tile_color'].setValue(self.backdrop_bg_color)
        nuke.updateUI()
        return bd

    DOWN = 0
    LEFT = 1
    RIGHT = 2

    @classmethod
    def create_dot(cls, input_node, direction=DOWN, distance=1):
        return cls.create_node_with_offset(nuke.nodes.Dot, input_node, direction, distance)

    @classmethod
    def create_node_with_offset(cls, node_type, input_node, direction=DOWN, distance=1, **kwargs):
        node = node_type(inputs=input_node if isinstance(input_node, list) else [input_node], **kwargs)
        if isinstance(input_node, list):
            input_node = input_node[0]
        if direction == cls.DOWN:
            x = input_node.xpos() + (input_node.screenWidth() // 2) - (node.screenWidth() // 2)
            y = input_node.ypos() + input_node.screenHeight() + (cls.position_y_step * distance)
        elif direction == cls.LEFT:
            x = input_node.xpos() - (cls.position_x_step * distance) + (input_node.screenWidth() // 2)
            y = input_node.ypos() + (input_node.screenHeight() // 2) - (node.screenHeight() // 2)
        elif direction == cls.RIGHT:
            x = input_node.xpos() + (cls.position_x_step * distance) + (input_node.screenWidth() // 2)
            y = input_node.ypos() + (input_node.screenHeight() // 2) - (node.screenHeight() // 2)
        else:
            raise ValueError
        node.setXYpos(x, y)
        return node


class RGBALayer(Layer):
    """
    Слои с паттерном имени RGBA*
    """
    backdrop_name = 'RGBA'
    backdrop_bg_color = 2863311360
    include_patterns = (
        re.compile(r"^RGBA_.+"),
    )
    exclude_patterns = ()
    after_merge_nodes = ()
    merge_to_main = False
    local_post_nodes = True


class OtherLayer(Layer):
    """
    Другие слои
    """
    backdrop_name = 'OTHER'
    backdrop_bg_color = 1903281664
    include_patterns = (
        re.compile(r"^N$"),
        re.compile(r"^P$"),
        re.compile(r"^Z$"),
        re.compile(r"^depth$"),
        re.compile(r"^volume_opacity$"),
        re.compile(r"^shadow_matte$"),
    )
    merge_to_main = False
    merge = False


# class HDRLayer(Layer):
#     """
#     Слои с паттерном имени *_hdri_light
#     """
#     backdrop_name = 'HDRI Light'
#     backdrop_bg_color = 831087871
#     include_patterns = (
#         re.compile(r".*?hdri_light$"),
#     )


# class DefaultLayer(Layer):
#     """
#     Слои с паттерном имени *_default
#     """
#     backdrop_name = 'Default'
#     backdrop_bg_color = HDRLayer.backdrop_bg_color
#     include_patterns = (
#         re.compile(r".*?_default$"),
#     )


class EmissionLayer(Layer):
    """
    Слой emission
    """
    backdrop_name = 'Emission'
    backdrop_bg_color = 1270269695
    include_patterns = (
        re.compile(r"^emission$"),
    )


class LightLayer(Layer):
    backdrop_bg_color = 831087871


def create_layer_class(backdrop_name, include_patterns, exclude_patters=None):
    cls = type(backdrop_name + 'Layer',
               (LightLayer,),
               dict(
                   backdrop_name=backdrop_name,
                   include_patterns=include_patterns,
                   exclude_patters=exclude_patters or ()
               ))
    return cls


def generate_light_classes(layer_list):
    groups = defaultdict(list)
    for layer in layer_list:
        m = re.match(r"[\w\d]+?_([\w\d]+)", layer)
        if m:
            groups[m.group(1)].append(layer)

    classes = []
    for grp_name in groups:
        c = create_layer_class(_to_camel_case(grp_name), (re.compile(r".*?%s$" % grp_name),))()
        c.collect_layers(layer_list)
        classes.append(c)
    return classes


# def generate_light_classes(layer_list):
#     # include_light_pattern = re.compile(r".*?(?<!hdri)_light$")
#     include_light_pattern = re.compile(r".*?(?<!hdri)_light$")
#     exclude_light_pattern = re.compile(r"^RGBA_.+")
#     light_layers = [x for x in layer_list if include_light_pattern.match(x) and not exclude_light_pattern.match(x)]
#     groups = defaultdict(list)
#     for layer in light_layers:
#         m = re.match(r"[\w\d_]+?_([\w\d]+_light)", layer)
#         if m:
#             groups[m.group(1)].append(layer)
#
#     classes = []
#     for grp_name in groups:
#         classes.append(create_layer_class(_to_camel_case(grp_name), (re.compile(r".*?"+grp_name),)))
#     return classes


def _to_camel_case(name):
    camel = re.sub(r"_\w", lambda x: x.group(0).strip('_').upper(), name)
    return camel[0].title() + camel[1:]


def node_center(node):
    return node.xpos() + (node.screenWidth() // 2), node.ypos() + (node.screenHeight() // 2)


def align_nodes_x(node, to_node):
    node.setXpos(to_node.xpos() + (to_node.screenWidth() // 2) - (node.screenWidth() // 2))


def align_nodes_y(node, to_node):
    node.setYpos(to_node.ypos() + (to_node.screenHeight() // 2) - (node.screenHeight() // 2))


class NotGeneratedError(Exception):
    pass


class AlreadyGeneratedError(Exception):
    pass
