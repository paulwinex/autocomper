from autocomper.layers import *
from fnmatch import fnmatch
import traceback
import nuke


def get_backdrop_order():
    # порядок бекдропов в скрипте слева направо, потом идут лайты
    return [
        RGBALayer,
        None,
        OtherLayer,
        EmissionLayer,
    ]


def check_intersections(classes):
    import itertools
    errors = []
    for comb in itertools.combinations(classes, 2):
        l1, l2 = comb
        inters = set(l1.layers).intersection(set(l2.layers))
        if inters:
            errors.append(f"{l1} <> {l2}: {inters}")
    if errors:
        raise NameError('LAYERS CONFLICT!: \n%s' % '\n'.join(errors))


def get_layer_classes(layers):
    classes = []
    for cls in get_backdrop_order():
        if cls is None:
            light_classes = generate_light_classes(layers)
            classes.extend(light_classes)
        else:
            c = cls()
            c.collect_layers(layers)
            layers = [x for x in layers if x not in c.layers]
            classes.append(c)
    check_intersections(classes)
    return classes


def unselect_all():
    for node in nuke.selectedNodes():
        node['selected'].setValue(False)


def filter_global_layer_list(layers):
    ignore_list = ['crypto*']
    result = []
    for lr in layers:
        if any([fnmatch(lr, pat) for pat in ignore_list]):
            continue
        result.append(lr)
    return result


def get_node_layers(node):
    chan = node.channels()
    layers = [c.rsplit('.')[0] for c in chan]
    return list(set(layers))


def add_post_nodes(output_node, src_node):
    # post nodes
    # keep RGB
    rem = Layer.create_node_with_offset(nuke.nodes.Remove, output_node, channels='rgb', operation='keep')
    # copy alpha and depth
    dot1 = Layer.create_dot(src_node, direction=Layer.LEFT)
    prev = rem
    dot2 = None
    for post_node in (
        dict(type=nuke.nodes.Copy,
             params=dict(from0='rgba.alpha', to0='rgba.alpha'),
             connect_root=True
             ),
        dict(type=nuke.nodes.Copy,
             params=dict(from0='rgba.red', to0='depth.Z'),
             connect_root=True),
        dict(type=nuke.nodes.ZDefocus2,
             params=dict(math='depth')),

    ):
        node = Layer.create_node_with_offset(post_node['type'], prev, **post_node.get('params', {}))
        if post_node.get('connect_root'):
            dot2 = Layer.create_dot(dot2 or dot1)
            align_nodes_y(dot2, node)
            node.setInput(1, dot2)
        prev = node


def create_precomp(node):
    # get layers
    node_layers = filter_global_layer_list(get_node_layers(node))
    # generate backdrops
    layer_classes = get_layer_classes(node_layers)
    layer_classes = [cls for cls in layer_classes if cls.layers]
    start_pos = None
    dot = node

    for i, cls in enumerate([x for x in layer_classes if x.layers]):
        if i > 0:
            prev = layer_classes[i-1]
            node = prev.root
            cls.create_precomp_nodes(node, offset=prev.lines+1)
        else:
            cls.create_precomp_nodes(node)
    # merge all
    step = 0
    last_merge = None
    main_merge = [cls for cls in layer_classes if cls.merge_to_main]
    for i, cls in enumerate(main_merge):
        step += 1
        if i == 0:
            prev_merge = last_merge = cls.output_node
        else:
            dot = cls.create_dot(cls.output_node, distance=i)
            m = prev_merge = cls.create_node_with_offset(nuke.nodes.Merge, [prev_merge, dot], operation='plus')
            dot.setYpos(node_center(m)[1]-(dot.screenHeight()//2))
            last_merge = m
    add_post_nodes(last_merge, layer_classes[0].root)

    for cls in layer_classes:
        if not cls.merge_to_main and cls.local_post_nodes:
            add_post_nodes(cls.output_node, layer_classes[0].root)

    unselect_all()

    return layer_classes


def main():
    try:
        node = nuke.selectedNode()
        create_precomp(node)
    except Exception as e:
        traceback.print_exc()
        nuke.message(str(e))